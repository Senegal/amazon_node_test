/**
 * AmazonController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
const util = require('util'),
      OperationHelper = require('apac').OperationHelper;

const opHelper = new OperationHelper({
  awsId:     'AKIAIACD5VJI56JSFP6Q',
  awsSecret: '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',
  assocId:   'mobilea097188-20'
});

const amazonRequest = (product_id) => {
  return new Promise( (resolve, reject) => {
    opHelper.execute('ItemLookup', {
      ItemId: product_id
    }, (err, results) => { 
      if(results['ItemLookupResponse']['Items']['Item']){
        resolve(results['ItemLookupResponse']['Items']['Item']['ItemAttributes']);
      }else{
        reject('not found');
      }
    });
  });
};

module.exports = {
  
  find: (req, res) => {
    
    amazonRequest(req.param('product'))
      .then(result => res.view('amazon/find', {data: result}) )
      .catch(err => res.view('amazon/find', {error: err}) );

  }
}

