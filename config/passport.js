  const passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  GoogleStrategy = require('passport-google-oauth20').Strategy;
  bcrypt = require('bcrypt-nodejs');

  passport.serializeUser(function(user, cb) {
    cb(null, user.id);
  });
  passport.deserializeUser(function(id, cb){
    User.findOne({id}, function(err, user) {
      cb(err, user);
    });
  });
  
  passport.use(new LocalStrategy({
      usernameField: 'username',
      passportField: 'password'
    }, function(username, password, cb){
      User.findOne({username: username}, function(err, user){
        if(err) return cb(err);
        if(!user) return cb(null, false, {message: 'Username not found'});
        bcrypt.compare(password, user.password, function(err, res){
          if(!res) return cb(null, false, { message: 'Invalid Password' });
          return cb(null, user, { message: 'Login Succesful'});
        });
      });
  }));


passport.use(new GoogleStrategy({
    clientID: '218264376530-p2v87lmc3m77njto7rhqlqp9qec79sd0.apps.googleusercontent.com',
    clientSecret: 'b6HQPtcxxs8wD5yCHyCEPX_X',
    callbackURL: "http://localhost:1337/auth/google/callback"
  },
  function(accessToken, refreshToken, profile, cb) {
    User.findOrCreate({ googleId: profile.id },{username:profile.id,email:'test',password:'test'}).exec(function (err, user) {
      return cb(err, user);
    });
  }
));